/*
**  main.c
**
**  Utilisation des fonctions du mini SGF.
**
**  04/04/2007
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sgf-disk.h"
#include "sgf-fat.h"
#include "sgf-dir.h"
#include "sgf-io.h"

int main(int argc, char **argv) {
    OFILE *file;
    int c, compteur = 0;

    init_sgf();

    if (argc < 2) {
        printf("\n Pour utiliser le programme voici les options:\n");
        printf("\t-d pour avoir le listing du disque\n");
        printf("\t-append pour tester la partie 5 du tp\n");
        printf("\t-seek pour tester la partie 3\n");
        printf("\t-puts pour tester la partie 4\n");
        printf("\t-rm pour tester le remove\n");
        printf("\t-wr pour tester le write\n");
        return (EXIT_SUCCESS);
    }

    if (strcmp(argv[1], "-d") == 0) {
        printf("\nListing du disque\n\n");
        list_directory();
    }

    if (strcmp(argv[1], "-rm") == 0) {
        int nb_inode;
        printf("\nListing du disque\n\n");
        list_directory();
        printf("\tajout d'un fichier\n");
        file = sgf_open("essai_remove.txt", WRITE_MODE);
        nb_inode = file->inode;
        sgf_putc(file, 'H');
        sgf_close(file);
        printf("\nListing du disque\n\n");
        list_directory();
        printf("\tsuppresion du fichier\n");
        sgf_remove(nb_inode);
        printf("\nListing du disque\n\n");
        list_directory();
    }

    /* Partie 5 */
    if (strcmp(argv[1], "-append") == 0) {
        /** TEST APPEND_MODE **/
        file = sgf_open("essai_APPEND.txt", WRITE_MODE);
        sgf_close(file);
        int i;
        for (i = 0; i < 500; ++i) {
            file = sgf_open("essai_APPEND.txt", APPEND_MODE);
            sgf_putc(file, 'H');
            sgf_close(file);
        }

        file = sgf_open("essai_APPEND.txt", READ_MODE);
        while ((c = sgf_getc(file)) > 0) {
            putchar(c);
        }
        printf("\n");
        sgf_close(file);
        printf("Fin test\n");
    }

    /* Parie 3 */
    if (strcmp(argv[1], "-seek") == 0) {
        file = sgf_open("essai.txt", READ_MODE);
        while ((c = sgf_getc(file)) > 0) {
            compteur += 8;
            putchar(c);
            sgf_seek(file, compteur);
        }
        sgf_close(file);
    }

    /* Parie 4 */
    if (strcmp(argv[1], "-puts") == 0) {
        file = sgf_open("essai.txt", WRITE_MODE);
        sgf_puts(file, "Ceci est un petit texte qui occupe\n");
        sgf_puts(file, "quelques blocs sur ce disque fictif.\n");
        sgf_puts(file, "Le bloc faisant 128 octets, il faut\n");
        sgf_puts(file, "que je remplisse pour utiliser\n");
        sgf_puts(file, "plusieurs blocs.\n");
        sgf_close(file);

        file = sgf_open("essai.txt", READ_MODE);
        while ((c = sgf_getc(file)) > 0) {
            putchar(c);
        }
        sgf_close(file);
    }
    /* Partie 6 */
    if (strcmp(argv[1], "-wr") == 0) {
        file = sgf_open("essai_WRITE.txt", WRITE_MODE);
        char *string;
        string = "Le calme ? oooooooooouuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"
                "uuuuuuuiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"
                "ouuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"
                "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"
                "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"
                "merci d'avoir repondu a la question";
        sgf_write(file, string, sizeof(string) - 1);
        printf(string);
        sgf_close(file);

        file = sgf_open("essai_WRITE.txt", READ_MODE);
        while ((c = sgf_getc(file)) > 0) {
            putchar(c);
        }
        sgf_close(file);
        printf("\n");
    }

    return (EXIT_SUCCESS);
}

